(function ($) {
    "use strict";
    var GO = {};
    GO.ele = {
        body: $("body"),
        scroll: $(".fullpage"),
        fullSection: $(".fullpage .section"),
        player: $(".player"),
        audio: $("audio"),
        loader: $(".loader")
    };
    GO.sectionID = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0];
    GO.bgColor = ["#171a17", "#fff"];
    GO.showImg = function (collec, type) {
        collec.each(function (i, e) {
            if (type) {
                $(this).delay((i + 1) * 500).fadeIn();
            } else {
                $(this).fadeIn();
            }
        });
    };
    GO.showScrollSection = function (index) {
        GO.showImg(GO.ele.fullSection.eq(index).find(".ele"), 1);
    };
    GO.onLeave = function (index, nextIndex, direction) {
        GO.ele.fullSection.find(".ele").fadeOut(200);
    };
    GO.onLoad = function (anchorLink, index) {
        GO.ele.body.css("background-color", GO.bgColor[GO.sectionID[index - 1]]);
        GO.showScrollSection(index - 1);
    };
    GO.onPause = function () {
        var aim = GO.ele.audio[0];
        if (aim.paused) {
            aim.play();
            GO.ele.player.addClass("rotate");
        } else {
            aim.pause();
            GO.ele.player.removeClass("rotate");
            GO.ele.player.css("transform", "rotate(0deg)");
        }
    };
    GO.onReady = function () {
        GO.ele.loader.hide();
    };
    GO.ele.body.ready(GO.onReady);
    GO.ele.player.click(GO.onPause);
    GO.ele.scroll.fullpage({
        sectionSelector: ".fullpage .section",
        resize: true,
        onLeave: GO.onLeave,
        afterLoad: GO.onLoad
    });
}(window.jQuery));